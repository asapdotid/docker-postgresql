#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username postgres <<-EOSQL
	CREATE USER halaluser WITH ENCRYPTED PASSWORD 'h4ru5H4l@lyAituBr)';
	CREATE DATABASE halalnodedb;
	GRANT ALL PRIVILEGES ON DATABASE halalnodedb TO halaluser;
EOSQL